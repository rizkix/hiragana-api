package model

import (
	"errors"

	"github.com/jinzhu/gorm"
)

type Katakana struct {
	Romawi string `json:"romawi"`
	Kanji  string
	Points string `json:"points"`
	gorm.Model
}

type KatakanaVM struct {
	Romawi string `json:"romawi"`
	Kanji  string `json:"kanji"`
	Points string `json:"points"`
	ID     int    `json:"id"`
}

func (this *KatakanaVM) GetItem(romawi string) error {
	db.Model(&Katakana{}).Select("romawi, kanji, points, id").Where("romawi = ?", romawi).Scan(&this)
	if this.ID == 0 {
		return errors.New("No data found")
	}
	return nil
}

func (this *Katakana) Save() error {
	if this.ID == 0 {
		db.Create(&this)
	} else {
		db.Save(&this)
	}
	return nil
}
